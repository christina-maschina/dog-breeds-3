package hr.tvz.android.mvpvidakovic.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DogBreeds implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("pathImage")
    private String imagePath;

    public DogBreeds(){}

    public DogBreeds(String name, String description, String images) {
        this.name = name;
        this.description = description;
        this.imagePath = images;
    }

    protected DogBreeds(Parcel in) {
        name = in.readString();
        description = in.readString();
        imagePath = in.readString();
    }

    public static final Creator<DogBreeds> CREATOR = new Creator<DogBreeds>() {
        @Override
        public DogBreeds createFromParcel(Parcel in) {
            return new DogBreeds(in);
        }

        @Override
        public DogBreeds[] newArray(int size) {
            return new DogBreeds[size];
        }
    };

    @Override
    public String toString() {
        return "DogBreeds{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", images=" + imagePath +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(imagePath);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public static Creator<DogBreeds> getCREATOR() {
        return CREATOR;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
