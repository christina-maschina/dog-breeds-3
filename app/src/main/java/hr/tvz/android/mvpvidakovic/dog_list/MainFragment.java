package hr.tvz.android.mvpvidakovic.dog_list;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


import hr.tvz.android.mvpvidakovic.adapter.MyRecyclerViewAdapter;
import hr.tvz.android.mvpvidakovic.R;
import hr.tvz.android.mvpvidakovic.receiver.SystemReceiver;
import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class MainFragment extends Fragment implements DogListContract.View {
    private static final String TAG = "MainFragment";
    private RecyclerView recyclerView;
    private MyRecyclerViewAdapter adapter;
    private Context context;
    private List<DogBreeds> dogList;
    private DogListPresenter dogListPresenter;
    public OnItemSelectedListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(context instanceof OnItemSelectedListener)) {
            throw new ClassCastException(context.toString() + " must implement listener");
        }
        listener = (OnItemSelectedListener) context;
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        dogList = new ArrayList<>();

        adapter = new MyRecyclerViewAdapter(((MainActivity) context).getSupportFragmentManager(),
                dogList, (MainActivity) context, listener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        initSystemReceiver();

        dogListPresenter=new DogListPresenter(this);
        dogListPresenter.requestDataFromServer();

        return view;
    }

    private void initSystemReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        requireActivity().registerReceiver(new SystemReceiver(), intentFilter);
    }

    @Override
    public void setDataToRecyclerView(List<DogBreeds> dogBreedsArrayList) {
        dogList.addAll(dogBreedsArrayList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResponseFailure(Throwable t) {
        Log.e(TAG, t.getMessage());
    }

    public interface OnItemSelectedListener {
        void OnItemSelected(DogBreeds dogBreeds);
    }
}
