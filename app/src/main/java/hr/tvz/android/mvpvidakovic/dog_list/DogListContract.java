package hr.tvz.android.mvpvidakovic.dog_list;

import java.util.List;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public interface DogListContract {

    interface Model{
        interface OnFinishedListener{
            void onFinished(List<DogBreeds> dogArrayList);
            void onFailure(Throwable t);
        }

        void getDogBreedsList(OnFinishedListener listener);
    }

    interface View{
        void setDataToRecyclerView(List<DogBreeds> dogBreedsArrayList);
        void onResponseFailure(Throwable t);

    }

    interface Presenter{
        void requestDataFromServer();

    }
}
