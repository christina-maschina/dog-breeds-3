package hr.tvz.android.mvpvidakovic.image;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;

import hr.tvz.android.mvpvidakovic.R;

public class ImageActivity extends AppCompatActivity {
    private Animation animation;
    private ImageView imageView;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        animation = AnimationUtils.loadAnimation(this, R.anim.animation);
        imageView=findViewById(R.id.img);
        imagePath=getIntent().getStringExtra("image");
        SimpleDraweeView draweeView = (SimpleDraweeView)findViewById(R.id.img);
        draweeView.setImageURI(Uri.parse(imagePath));
        imageView.startAnimation(animation);
    }
}