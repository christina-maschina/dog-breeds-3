package hr.tvz.android.mvpvidakovic.fresco;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class FrescoInitializing extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
