package hr.tvz.android.mvpvidakovic.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;

import java.io.IOException;

import hr.tvz.android.mvpvidakovic.R;

public class DogWidgetProvider extends AppWidgetProvider {
    public static final String UPDATE_WIDGET = "DogWidgetProvider";
    public static final String NAME_EXTRA = "name";
    public static final String IMAGE_EXTRA = "image";

    String name;
    String image;
    Uri uri;
    Handler handler = new Handler();

    @Override
    public void onReceive(Context context, Intent intent) {
        name = intent.getStringExtra(NAME_EXTRA);
        image = intent.getStringExtra(IMAGE_EXTRA);
        if (image != null) {
            uri = Uri.parse(image);
        }

        if (UPDATE_WIDGET.equals(intent.getAction())) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisAppWidget = new ComponentName(context.getOpPackageName(),
                    DogWidgetProvider.class.getName());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
            try {
                refresh(context, appWidgetManager, appWidgetIds);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        try {
            refresh(context, appWidgetManager, appWidgetIds);
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private void refresh(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) throws IOException {
        for (int appWidgetId : appWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.dog_widget);
            remoteViews.setTextViewText(R.id.text_widget, name);
            if (uri != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Picasso.get()
                                .load(uri)
                                .resize(200, 200)
                                .into(remoteViews, R.id.img_widget, new int[]{appWidgetId});
                    }
                });
            }
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);

        }

    }
}
