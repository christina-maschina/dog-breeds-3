package hr.tvz.android.mvpvidakovic.dog_details;

import android.os.Bundle;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class DogDetailsPresenter implements DogDetailsContract.Presenter, DogDetailsContract.Model.OnFinishedListener{
    private DogDetailsModel dogDetailsModel;
    private DogDetailsContract.View dogDetailsView;

    public DogDetailsPresenter(DogDetailsContract.View dogDetailsView){
        this.dogDetailsView=dogDetailsView;
        this.dogDetailsModel=new DogDetailsModel();
    }

    @Override
    public void requestDogData(Bundle bundle) {
        dogDetailsModel.getDogDetails(this, bundle);
    }

    @Override
    public void onFinished(DogBreeds dogBreeds) {
        dogDetailsView.setDataToView(dogBreeds);
    }

    @Override
    public void onFailure(String error) {
        dogDetailsView.onResponseFailure(error);
    }
}
