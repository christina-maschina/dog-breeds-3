package hr.tvz.android.mvpvidakovic.dog_details;

import android.content.Context;
import android.os.Bundle;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public interface DogDetailsContract {
    interface Model{

        interface OnFinishedListener{
            void onFinished(DogBreeds dogBreeds);
            void onFailure(String error);
        }

        void getDogDetails(OnFinishedListener listener, Bundle bundle);

    }
    interface View{

        void setDataToView(DogBreeds dataToView);
        void onResponseFailure(String error);

    }

    interface Presenter{
        void requestDogData(Bundle bundle);
    }
}
