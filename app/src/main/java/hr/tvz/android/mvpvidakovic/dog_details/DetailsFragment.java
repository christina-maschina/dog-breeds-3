package hr.tvz.android.mvpvidakovic.dog_details;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.Objects;

import hr.tvz.android.mvpvidakovic.image.ImageActivity;
import hr.tvz.android.mvpvidakovic.R;
import hr.tvz.android.mvpvidakovic.dialog.ShareDialog;
import hr.tvz.android.mvpvidakovic.model.DogBreeds;
import hr.tvz.android.mvpvidakovic.widget.DogWidgetProvider;

import static hr.tvz.android.mvpvidakovic.widget.DogWidgetProvider.IMAGE_EXTRA;
import static hr.tvz.android.mvpvidakovic.widget.DogWidgetProvider.NAME_EXTRA;

public class DetailsFragment extends Fragment implements DogDetailsContract.View {
    private final String TAG = "DetailsFragment";
    private TextView titleView, descriptionView;
    private ImageView imageView;
    private RatingBar ratingBar;
    float userRating;
    private String imagePath;
    private String dogName;
    private Context context;
    public OnCallbackReceived mCallback;
    private DogDetailsPresenter dogDetailsPresenter;
    private View view;

    public DetailsFragment() { }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            mCallback = (OnCallbackReceived) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setHasOptionsMenu(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, container, false);

        initUI(view);
        sendRating();

        dogDetailsPresenter = new DogDetailsPresenter(this);
        dogDetailsPresenter.requestDogData(getArguments());

        openImage();
        sendDataToWidget();

        return view;
    }

    private void sendDataToWidget() {
        Intent intent = new Intent(getContext(), DogWidgetProvider.class);
        intent.setAction(DogWidgetProvider.UPDATE_WIDGET);
        intent.putExtra(NAME_EXTRA, dogName);
        intent.putExtra(IMAGE_EXTRA, imagePath);
        getActivity().sendBroadcast(intent);
    }

    private void initUI(View view) {
        titleView = view.findViewById(R.id.title);
        descriptionView = view.findViewById(R.id.description);
        imageView = view.findViewById(R.id.img);
        ratingBar = view.findViewById(R.id.ratingBar);
    }

    private void sendRating() {
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mCallback.send(rating);
            }
        });
    }

    private void openImage() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ImageActivity.class);
                intent.putExtra("image", imagePath);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button btn = (Button) Objects.requireNonNull(getActivity()).findViewById(R.id.button);
        if (btn != null) {
            btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String url = "https://www.countryliving.com/life/kids-pets/g3283/the-50-most-popular-dog-breeds/";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(context, "Can't display web page", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.activity_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            if (drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        DialogFragment shareDialog = new ShareDialog(getActivity(), userRating);
        shareDialog.show(getActivity().getSupportFragmentManager(), "Share");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setDataToView(DogBreeds dogBreeds) {
        imagePath = dogBreeds.getImagePath();
        dogName = dogBreeds.getName();

        if (dogBreeds != null) {
            titleView.setText(dogName.toUpperCase());
            descriptionView.setText(dogBreeds.getDescription());

            SimpleDraweeView draweeView = (SimpleDraweeView) view.findViewById(R.id.img);
            draweeView.setImageURI(Uri.parse(imagePath));
        }
    }

    @Override
    public void onResponseFailure(String error) {
        Log.d(TAG, error);
    }

    public interface OnCallbackReceived {
        void send(float userRating);
    }

}
