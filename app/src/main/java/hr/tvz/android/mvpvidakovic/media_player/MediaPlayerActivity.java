package hr.tvz.android.mvpvidakovic.media_player;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hr.tvz.android.mvpvidakovic.R;

public class MediaPlayerActivity extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ArrayList<String> songs;
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        songs = new ArrayList<>();
        fillTheList(songs);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, songs);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(MediaPlayerActivity.this, PlayMusicService.class);
                intent.putExtra("song", songs.get(position));
                startService(intent);
            }
        });

    }

    private void fillTheList(ArrayList<String> songs) {
        songs.add("https://www.mboxdrive.com/SiSe-%20Mariposa%20(en%20Havana).mp3");
        songs.add("https://www.mboxdrive.com/tito%20puente%20-%20Cha%20cha%20cha.mp3");
        songs.add("https://www.mboxdrive.com/Irakere%20-%20Concierto%20Para%20Metales.mp3");
        songs.add("https://www.mboxdrive.com/Jimmy%20Sax%20-%20No%20Man%20No%20Cry(live).mp3");
        songs.add("https://www.mboxdrive.com/A%20Song%20For%20YouRay%20Charles.mp3");
    }







  
  
  
  
  
  
}
