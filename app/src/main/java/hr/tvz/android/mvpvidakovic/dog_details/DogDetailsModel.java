package hr.tvz.android.mvpvidakovic.dog_details;

import android.os.Bundle;
import android.util.Log;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class DogDetailsModel implements DogDetailsContract.Model {
    private final String TAG = "DogDetailsModel";

    @Override
    public void getDogDetails(final OnFinishedListener listener, Bundle bundle) {
        if (bundle != null) {
            DogBreeds dogBreeds = bundle.getParcelable("dogBreeds");

            Log.d(TAG, "Dog details data received: " + dogBreeds.toString());

            listener.onFinished(dogBreeds);
        } else {
            listener.onFailure("Couldn't load dog details data");

        }

    }
}
