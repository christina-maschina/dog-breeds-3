package hr.tvz.android.mvpvidakovic.dog_list;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import hr.tvz.android.mvpvidakovic.dog_details.DetailsFragment;
import hr.tvz.android.mvpvidakovic.R;
import hr.tvz.android.mvpvidakovic.databinding.ActivityMainBinding;
import hr.tvz.android.mvpvidakovic.media_player.MediaPlayerActivity;
import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MainFragment.OnItemSelectedListener, DetailsFragment.OnCallbackReceived {
    public static String MY_CHANNEL = "myChannel";
    private final String LAYOUT="layout";
    private float userRating;
    private ActivityMainBinding binding;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        initUI();
        setSupportActionBar(toolbar);
        initDrawer();
        createNotificationChannel();
        getFirebaseToken();
    }

    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = binding.drawer;
        navigationView = binding.navigationView;
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void getFirebaseToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Main activity", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult();
                        Log.d("Main activity", token);
                    }
                });
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(MY_CHANNEL, "Notification channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Description");
            NotificationManager notificationManager = (NotificationManager) (getSystemService(Context.NOTIFICATION_SERVICE));
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void initDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //load default fragment
        if (findViewById(R.id.item_detail_container) == null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.item_list_container, new MainFragment());
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if (item.getItemId() == R.id.home) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            if (findViewById(R.id.item_detail_container) == null) {
                fragmentTransaction.replace(R.id.item_list_container, new MainFragment());
            }
            fragmentTransaction.commit();
        }
        if (item.getItemId() == R.id.media_player) {
            Intent intent = new Intent(this, MediaPlayerActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void OnItemSelected(DogBreeds dogBreeds) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("dogBreeds", dogBreeds);
        DetailsFragment detailFragment = new DetailsFragment();
        detailFragment.setArguments(arguments);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();

        if (findViewById(R.id.item_detail_container) != null) {
            Log.d(LAYOUT, "landscape");
            fragmentTransaction
                    .replace(R.id.item_detail_container, detailFragment)
                    .commit();
        } else {
            Log.d(LAYOUT, "portrait");
            fragmentTransaction
                    .addToBackStack(null)
                    .replace(R.id.item_list_container, detailFragment)
                    .commit();
        }
    }

    @Override
    public void send(float userRating) {
        this.userRating = userRating;
    }
}