package hr.tvz.android.mvpvidakovic.dog_list;
import android.util.Log;
import java.util.List;
import hr.tvz.android.mvpvidakovic.model.DogBreeds;
import hr.tvz.android.mvpvidakovic.network.ApiInterface;
import hr.tvz.android.mvpvidakovic.network.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DogListModel implements DogListContract.Model {
    private final String TAG = "DogListModel";

    @Override
    public void getDogBreedsList(final OnFinishedListener listener) {
        ApiInterface service = ServiceGenerator.createService(ApiInterface.class);
        Call<List<DogBreeds>> call = service.getAll();
        call.enqueue(new Callback<List<DogBreeds>>() {
            @Override
            public void onResponse(Call<List<DogBreeds>> call, Response<List<DogBreeds>> response) {
                List<DogBreeds> dogs = response.body();
                Log.d(TAG, "Number of dogs received: " + dogs.size());
                listener.onFinished(dogs);
            }

            @Override
            public void onFailure(Call<List<DogBreeds>> call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onFailure(t);
            }
        });

    }
}
