package hr.tvz.android.mvpvidakovic.dog_list;

import java.util.List;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class DogListPresenter implements DogListContract.Presenter, DogListContract.Model.OnFinishedListener{

    private DogListContract.View dogListView;
    private DogListModel dogListModel;

    public DogListPresenter(DogListContract.View view) {
        this.dogListView=view;
        this.dogListModel= new DogListModel();
    }

    @Override
    public void onFinished(List<DogBreeds> dogArrayList) {
        dogListView.setDataToRecyclerView(dogArrayList);
    }

    @Override
    public void onFailure(Throwable t) {
        dogListView.onResponseFailure(t);
    }

    @Override
    public void requestDataFromServer() {
        dogListModel.getDogBreedsList(this);
    }
}
