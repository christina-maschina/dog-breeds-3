package hr.tvz.android.mvpvidakovic.network;

import java.util.List;

import hr.tvz.android.mvpvidakovic.model.DogBreeds;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("dogs")
    Call<List<DogBreeds>> getAll();

}
