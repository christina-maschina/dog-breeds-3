package hr.tvz.android.mvpvidakovic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hr.tvz.android.mvpvidakovic.R;
import hr.tvz.android.mvpvidakovic.dog_list.MainActivity;
import hr.tvz.android.mvpvidakovic.dog_list.MainFragment;
import hr.tvz.android.mvpvidakovic.model.DogBreeds;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<DogBreeds> dogList;
    private Context context;
    public FragmentManager fragmentManager;
    private View details;
    private MainFragment.OnItemSelectedListener listener;

    public MyRecyclerViewAdapter(FragmentManager manager, List<DogBreeds> dogBreeds, MainActivity context, MainFragment.OnItemSelectedListener listener) {
        this.fragmentManager = manager;
        this.context = context;
        this.dogList=dogBreeds;
        details = context.findViewById(R.id.item_detail_container);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        setColor(holder, position);
        holder.dogNameText.setText(dogList.get(position).getName().toUpperCase());

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pressedColor = setColor(holder, position + 1);
                holder.cardView.setCardBackgroundColor(pressedColor);
                DogBreeds dogBreeds = dogList.get(position);
                listener.OnItemSelected(dogBreeds);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dogList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView dogNameText;
        ConstraintLayout mainLayout;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            dogNameText = itemView.findViewById(R.id.dog);
            cardView = itemView.findViewById(R.id.card);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }

    }

    private int setColor(ViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.baby_blue));
            return ContextCompat.getColor(context, R.color.baby_blue);
        }
        holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.baby_green));
        return ContextCompat.getColor(context, R.color.baby_green);
    }


}
