package hr.tvz.android.mvpvidakovic.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import hr.tvz.android.mvpvidakovic.R;
import hr.tvz.android.mvpvidakovic.dog_list.MainActivity;

public class NotificationFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = " FCM Service";
    private final String URL ="https://www.goodhousekeeping.com/life/pets/g23064807/rare-dog-breeds/";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Notification.Builder builder = new Notification.Builder(
                getApplicationContext(), MainActivity.MY_CHANNEL)
                .setSmallIcon(R.drawable.ic_baseline_notifications_active_24)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.opne_in_new))
                .setContentTitle("Upday")
                .setColor(getColor(android.R.color.holo_blue_light))
                .setContentText("See more unusual types of dogs");

        Intent resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "To: " + remoteMessage.getTo());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
    }
}
