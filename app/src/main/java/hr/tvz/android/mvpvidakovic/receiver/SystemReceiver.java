package hr.tvz.android.mvpvidakovic.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.widget.Toast;

public class SystemReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(AudioManager.ACTION_HEADSET_PLUG)){
            int state=intent.getIntExtra("state", -1);
            switch (state){
                case 0:
                    Toast.makeText(context, "Headset is unplugged", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(context, "Headset is plugged", Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(context, "I have no idea what headset state is", Toast.LENGTH_LONG).show();
            }

        }
    }
}
