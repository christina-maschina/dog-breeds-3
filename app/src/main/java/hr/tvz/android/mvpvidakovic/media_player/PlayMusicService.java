package hr.tvz.android.mvpvidakovic.media_player;

import android.app.Service;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;

public class PlayMusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{
    private MediaPlayer mediaPlayer;
    private String song;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioAttributes(new AudioAttributes
                .Builder()
                .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                .build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        song=intent.getStringExtra("song");

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            Log.d("song", "stop");
        } else {
            Log.d("song", "play");
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(song);
            } catch (IOException e) {
                Log.e("Online player", e.getMessage(), e);
            }
            mediaPlayer.setOnPreparedListener(PlayMusicService.this::onPrepared);
            mediaPlayer.prepareAsync();
        }
        mediaPlayer.setOnErrorListener(this);
        return START_NOT_STICKY;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mp.reset();
        Log.e("Online player", "Što: " + what + "/ ekstra: " + extra);
        return false;
    }
}
